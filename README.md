# Daphnias

## General
This python script is designed to count the number of animals in a short video clip. The script will use a pre-trained U-Net network that detects the animals and then counts them using cv2's goodFeaturesToTrack.
## Assumptions
The video is .AVI formatted
Right-most and left-most strips are expected to have false-positives such as reflections, hence the signal on these strips is artificially hampered
## Dependencies
This script runs on Python 3 and relies on the following packages/recommended versions:
-   OpenCV 4.2.0
-   Tensorflow 1.13.1
-   Keras 2.1.6
-   scipy 1.1.0
-   numpy 1.18.1
-   pandas 0.24.2
-   tqdm 4.42.1
-   matplotlib 3.1.3
## Authors
ANNA under supervision of Leon 

## Usage

TO-RUN: ```python count_daphnias.py -p PATH -o OUT -i IMAGES [-m MODEL]```

INPUT: 
-   videos inside the `PATH` directory;
-   *.h5 model by the `MODEL` path (big_model.h5 by default)

OUTPUT: 
-   `OUT` file with number of detects for each video and their 95% confidence intervals;
-   `%IMAGES%/%FILENAME%_%FRAME%_%DETECTS%.png` with detects marked with red dots on 10 frames of each video

## Further development
The script `count_daphnias_first_50.py` counts the results on the first 50 frames and it can be seen that it detects daphnias rather robustly. If we could connect these per-frame detects into best-explaining-paths we could improve both the counting task and tracking task (which is also needed) all together. This type of task, i.e. detection-based multi-object tracking, is an often occurence in video surveillence image processing so I suggest looking into their papers and trying to implement some of their approaches / methods, for instance this https://sci-hub.tw/10.1109/BigMM.2015.19 (Detection and Association based Multi-target Tracking in Surveillance Video, 2015) or that https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007698

You might also take a look at this link https://github.com/SpyderXu/multi-object-tracking-paper-list since some of the approaches are already implemented as open-source
