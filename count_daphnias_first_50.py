import argparse
parser = argparse.ArgumentParser(description='Process video files from a folder and detect daphnias')
parser.add_argument(
    '-p', '--path',
    default='.',
    help='folder where the videos are')
parser.add_argument(
    '-o', '--out',
    default='output.txt',
    help='path to file where to write the results')
parser.add_argument(
    '-i', '--images',
    default='images',
    help='path where to save images')
parser.add_argument(
    '-m', '--model',
    default='big_model.h5',
    help='path to the model')
parser.add_argument(
    '-c', '--cpu',
    action='store_true',
    default=False,
    help='force CPU usage')
args = vars(parser.parse_args())

import scipy
from scipy import stats
import numpy as np
import pandas as pd
from glob import glob
import cv2

import os

import warnings
warnings.filterwarnings('ignore', category=DeprecationWarning)
warnings.filterwarnings('ignore', category=FutureWarning)

from keras.backend import tf
if args['cpu']:
    print('USE CPU')
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

from tqdm.auto import tqdm
from keras.models import load_model
from scipy.ndimage.measurements import label, center_of_mass

import keras.backend as K

# Define IoU metric
def mean_iou(y_true, y_pred):
    prec = []
    for t in np.arange(0.5, 1.0, 0.05):
        y_pred_ = tf.to_int32(y_pred > t)
        score, up_opt = tf.metrics.mean_iou(y_true, y_pred_, 2)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([up_opt]):
            score = tf.identity(score)
        prec.append(score)
    return K.mean(K.stack(prec), axis=0)

dependencies = {
    'mean_iou': mean_iou
}

model = load_model(args['model'], custom_objects=dependencies)

movies = []
for frmt in ['mp4', 'avi', 'mov', 'mpeg', 'flv', 'wmv']:
    movies.extend(glob(os.path.join(args['path'], '*.%s' % frmt)))

f = open(args['out'], 'w')

if not os.path.exists(args['images']):
    os.mkdir(args['images'])

for movie_path in movies:
    movie = os.path.basename(movie_path)
    cap = cv2.VideoCapture(movie_path)
    frame_number = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    print('Loading %s...' % (movie))
    first_few_frames = []
    for j in tqdm(range(frame_number)):
        ret,frame = cap.read()
        first_few_frames.append(frame[:,:,0])
        del frame

    first_few_frames = np.array(first_few_frames)
    shape = first_few_frames[0].shape

    print('Calculating daphnias...')
    # detect vertical mirrors
    b = first_few_frames[0].mean(axis=0)
    cdiff = np.abs(b - np.roll(b,2))
    large_cdiff = cdiff > np.percentile(cdiff, 95)
    strip_left = max(np.where(large_cdiff[:len(cdiff)//2])[0]) - 10
    strip_right = min(np.where(large_cdiff[len(cdiff)//2:])[0] + len(cdiff)//2) + 10

    zoom = 1

    cnts = []
    sigs = []
    for cf in tqdm(range(0, 50)):#frame_number, frame_number // 45)):
        img = np.expand_dims(first_few_frames[cf], axis=2)
        # was -20 +20
        diff = np.expand_dims(first_few_frames[max(0,cf-40):cf+40].mean(axis=0) - first_few_frames[cf], axis=2)

        # anti-reflection:
        diff[:,:strip_left,0]/=5 # 15
        diff[:,strip_right:,0]/=5 # 15

        pred_from = [img, diff]
        for d in range(2):
            pred_from1 = cv2.resize(pred_from[d], dsize=(int(zoom * img.shape[1]), int(zoom * img.shape[0])))
            pred_from[d] *= 0
            if pred_from1.shape[0] < pred_from[d].shape[0]:
                w0, h0 = pred_from[d].shape[0]//2 - pred_from1.shape[0]//2, \
                         pred_from[d].shape[1]//2 - pred_from1.shape[1]//2
                pred_from[d][w0:w0 + pred_from1.shape[0], h0:h0 + pred_from1.shape[1], 0] = pred_from1
            else:
                w0, h0 = pred_from1.shape[0]//2 - pred_from[d].shape[0]//2, \
                         pred_from1.shape[1]//2 - pred_from[d].shape[1]//2
                pred_from[d][:,:,0] = pred_from1[w0:w0 + pred_from[d].shape[0], h0:h0 + pred_from[d].shape[1]]

        preds = model.predict(np.expand_dims(np.concatenate(pred_from, axis=2), axis=0))[0,:,:,0]
        feature_params = dict( maxCorners = 600,
                           qualityLevel = 0.10,
                           minDistance = 4,
                           blockSize = 4)#3 )
        p0 = cv2.goodFeaturesToTrack(preds, mask = None, **feature_params)

        from matplotlib import pyplot as plt
        fig = plt.figure(frameon=False)
        DPI = fig.get_dpi()
        fig.set_size_inches(img.shape[1]/float(DPI),img.shape[0]/float(DPI))
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)

        plt.imshow(np.repeat(pred_from[0], 3, axis=2))
        plt.scatter(p0[:,0,0], p0[:,0,1], s=2, color='red')
        plt.savefig('%s/%s_%s_%s.png' % (args['images'], movie, cf, len(p0)))
        plt.close()

        cnts.append(len(p0))
        sigs.append(np.sum(preds))

    d = pd.Series(sorted(cnts))
    d = d[abs(d - d.median()) < 30]
    s = pd.Series(sorted(sigs))
    factor1=(d.mean() / s.mean())
    factor2 = (d.median() / s.median())
    s *= d.mean() / s.mean()

    data_part = ((len(s) * 95 + 99)//100)
    print(f'real percentage is {data_part/len(s)}')
    right_side = s.rolling(data_part, min_periods=data_part).max()
    left_side = s.rolling(data_part, min_periods=data_part).min()
    i = (right_side - left_side).idxmin()

    mean, sigma = s.mean(), s.std()

    bottom25, top25 = left_side.iloc[i], right_side.iloc[i]
    bottom251, top251 = d.quantile(0.025), d.quantile(0.975)
    bottom5, top5 = s.quantile(0.05), s.quantile(0.95)
    conf_int = stats.norm.interval(0.95, loc=mean, scale=sigma)
    f.write('%s: %.0f, [%.1f, %.1f]\n' % (movie, np.round(s.mean()), max(0, conf_int[0]), conf_int[1])) #bottom25, top25, bottom251, top251))#np.round(d.mean()), bottom25, top25, bottom5, top5))#max(0, conf_int[0]), conf_int[1]))
    f.flush()

    del first_few_frames

f.close()
